using NatML.Recorders;
using NatML.Recorders.Clocks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;
using TMPro;
using System;

public class CaptureCamera : MonoBehaviour
{
    [Header("Display Data")]
    [SerializeField] private CanvasScaler canvasScaler;
    [SerializeField] private RawImage displayImage;
    [SerializeField] private TMP_Dropdown dropdown;
    [SerializeField] private TMP_Text TMPRecordingTime;
    [SerializeField] private Image BTNRecordImage;
    [SerializeField] private Sprite SPRRecord;
    [SerializeField] private Sprite SPRStopRecording;
    private WebCamDevice[] cam_devices;
    private bool hasCameraPermission = false;
    private WebCamTexture webcamTexture;

    [Header("Video Saver Data")]
    [SerializeField] private int w;
    [SerializeField] private int h;
    [SerializeField] private int f;
    private MP4Recorder recorder;
    private Coroutine recordVideoCoroutine;
    private bool startedRecording = false;


    void Start()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            CheckForCameraDevices();
        }
    }

    private void Update()
    {
        while (!hasCameraPermission)
        {
            CheckForCameraDevices();
        }
    }

    private void CheckForCameraDevices()
    {
        if (WebCamTexture.devices.Length > 0)
        {
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            else
            {
                cam_devices = WebCamTexture.devices;
                List<string> camNames = new List<string>();
                foreach (WebCamDevice webCamDevice in cam_devices)
                {
                    camNames.Add(webCamDevice.name.ToString());
                }
                dropdown.AddOptions(camNames);
                if (camNames.Count >= 1)
                {
                    webcamTexture = new WebCamTexture(cam_devices[1].name, w, h, f);
                    dropdown.value = 1;
                }
                else
                {
                    webcamTexture = new WebCamTexture(cam_devices[0].name, w, h, f);
                    dropdown.value = 0;
                }

                //displayImage.texture = webcamTexture;
                //displayImage.material.mainTexture = webcamTexture;
                webcamTexture.Play();
                hasCameraPermission = true;
            }
        }
    }

    public void ChangeCamera()
    {
        webcamTexture.Pause();
        webcamTexture = new WebCamTexture(cam_devices[dropdown.value].name, w, h, f);
        webcamTexture.Play();
    }

    public void OnRecordButtonPressed()
    {
        if (!startedRecording)
        {
            BTNRecordImage.sprite = SPRStopRecording;
            startRecording();
        }
        else
        {
            BTNRecordImage.sprite = SPRRecord;
            stopRecording();
        }
        startedRecording = !startedRecording;
    }

    public void startRecording()
    {
        // Create a recorder
        recorder = new MP4Recorder(width: w, height: h, frameRate: f);

        //Start recording
        recordVideoCoroutine = StartCoroutine(recording());
    }

    private IEnumerator recording()
    {
        var clock = new RealtimeClock();
        float initialTime = Time.time;

        while (true)
        {
            // Commit video frame
            recorder.CommitFrame(webcamTexture.GetPixels32(), clock.timestamp);

            // Show Recording Time in MM:SS format
            float recordingTime = Time.time - initialTime;
            int minutes = Mathf.FloorToInt(recordingTime / 60f); // Get whole minutes
            int seconds = Mathf.FloorToInt(recordingTime % 60f); // Get remaining seconds

            string formattedTime = string.Format("{0:00}:{1:00}", minutes, seconds);
            TMPRecordingTime.gameObject.SetActive(true);
            TMPRecordingTime.text = formattedTime;

            yield return new WaitForEndOfFrame();
        }
    }


    public async void stopRecording()
    {
        // Stop Showing Recording Time
        TMPRecordingTime.gameObject.SetActive(false);
        TMPRecordingTime.text = "00:00";

        //Stop Coroutine
        StopCoroutine(recordVideoCoroutine);
        // Finish writing
        var recordingPath = await recorder.FinishWriting();

        //Save video to gallery
        DateTime now = DateTime.Now;
        string videoName = now.ToString("yyyy-MM-dd_HH-mm-ss");
        NativeGallery.Permission permission = NativeGallery.SaveVideoToGallery(recordingPath, "Mobile Recording", videoName + ".mp4", (success, path) => Debug.Log("Media save result: " + success + " " + path));
    }
}
